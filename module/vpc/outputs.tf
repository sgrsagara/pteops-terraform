output "vpc_id" {
  value = "${aws_vpc.main.id}"
}

output "subnet_id" {
  value = "${aws_subnet.main.id}"
}

/*
output "subnet_ids" {
  value = [for s in data.aws_subnet.main_subs : s.id]
}
*/

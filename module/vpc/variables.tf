variable "vpc_name" {
    type = string
    default = "Intern_VPC"
}

variable "cidr_block" {
    type = string
    default = "10.0.0.0/16"
}

variable "tenancy" {
    type = string
    default = "default"
}

variable "subnet_cidr_block" {
  type = string
  default = "10.0.1.0/24"
}

variable "subnet_name" {
  default = "public"
}


resource "aws_vpc" "main" {
  cidr_block       = "${var.cidr_block}"
  instance_tenancy = "${var.tenancy}"

  tags = {
    Name = "${var.vpc_name}"
  }
}

resource "aws_subnet" "main" {
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "${var.subnet_cidr_block}"

  tags = {
    Name = "${var.subnet_name}"
  }
}

data "aws_subnet_ids" "subnets" {
  vpc_id = aws_vpc.main.id
}
/*
data "aws_subnet" "main_subs" {
  for_each = data.aws_subnet_ids.subnets.ids
  id       = each.value
}
*/
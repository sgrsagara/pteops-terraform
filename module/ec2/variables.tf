variable "ami_id" {}

variable "subnet_id" {
    type = string
}

variable "key_name" {
    type = string
    default = "key1"
}

variable "instance_type" {
    type = string
    default = "t2.micro"
}

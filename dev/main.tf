terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

terraform {
  backend "http" {
  }
}


provider "aws" {
    region = "ap-south-1"
}

module "opstools_dev" {
    source = "../module/vpc"
    cidr_block = "${var.cidr_block}"
    instance_tenancy = "${var.tenancy}"
}

module "servers" {
    source = "../module/ec2"
    ami           = "${var.ami_id}"
    instance_type = "${var.instance_type}"
    subnet_id = "${module.opstools_dev.subnet_id}"
    key = "${var.key_name}"
}


variable "vpc_name" {
    type = string
    default = "OpsTools-DEV-VPC"
}

variable "cidr_block" {
    type = string
    default = "10.0.0.0/16"
}

variable "tenancy" {
    type = string
    default = "default"
}

variable "subnet_cidr_block" {
  type = string
  default = "10.0.1.0/24"
}

variable "subnet_name" {
  default = "public"
}

variable "ami_id" {
    type = string
    default = "ami-0c1a7f89451184c8b"
}

variable "key_name" {
    type = string
    default = "key1"
}

variable "instance_type" {
    type = string
    default = "t2.micro"
}
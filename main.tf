terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

terraform {
  backend "http" {
  }
}


provider "aws" {
    region = "ap-south-1"
}

module "opstools_dev" {
    source = "./module/vpc"
    cidr_block = "${var.cidr_block}"
}

module "servers" {
    source = "./module/ec2"
    ami_id           = "${var.ami_id}"
    instance_type = "${var.instance_type}"
    subnet_id = "${module.opstools_dev.subnet_id}"
    count = "${var.subnet_count}"
    /* key = "${var.key_name}" */
}

resource "aws_iam_user" "apache" {
  name="apache"
}

resource "aws_instance" "web" {
    ami           = "${var.ami_id}"
    instance_type = "${var.instance_type}"
    subnet_id = "${module.opstools_dev.subnet_id}"

  provisioner "local-exec" {
    command = "echo The IP address is ${self.private_ip}"
  }
}

